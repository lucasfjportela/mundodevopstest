Desafio Mundo DevOps feito em React Native (versão 0.59.9).

Este projeto foi implementado para ser executado apenas na plataforma Android.

Após clonar o projeto, apenas um passo é necessário para executar o projeto:
	** Usando node v11.14.0 (npm v6.7.0) **

	- npm install

Todas as bibliotecas utilizadas no projeto já estão linkadas para a plataforma Android.

Após isso, executar o comando "react-native run-android" para começar o processo de build.

P.S: Algumas vezes podem ocorrer problemas internos no Metro Bundler após a finalização do build para qualquer tipo de projeto. Caso aconteça, para resolver basta executar o comandos a seguir:

	- npm cache clean --force && npm start --reset-cache


Implementação:

Para conduzir a implementação da tela, baseado no sketch enviado, utilizei as boas práticas de programação da linguagem em questão, tendo o cuidado de escrever todo o código de forma legível e performática.

Fiz algumas alterações no data.json como, por exemplo, adicionar a categoria de cada exercício para poder executar a ação de filtro. Nos botões inseri apenas um Alert para representar a ação de pressionar.

Funcionamento do filtro:

Caso nenhum filtro estiver selecionado todos os exercício serão exibidos. É possível selecionar mais de um filtro. 

Caso o filtro de exercício não possua nenhum exercício daquela categoria, uma mensagem é exibida informado ao usuário (filtro de Dança, por exemplo).
