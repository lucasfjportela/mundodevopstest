import React, { Component } from 'react'
import { StyleSheet, ScrollView } from 'react-native'  
import { filters } from 'mundodevopstest/src/data/filters'
import { exercises } from 'mundodevopstest/src/data/exercises'
import Header from 'mundodevopstest/src/components/header'
import Filters from 'mundodevopstest/src/components/filters'
import Exercises from 'mundodevopstest/src/components/exercises'

export default class Main extends Component {
  constructor (props) {
    super(props)

    this.state = {
    	selectedFilters: []
    }
  }

  onUpdate = (key, val) => {
    this.saveState(key, val)
  }

  saveState(k, v) {
    this.setState({ [k]: v })
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Header />

    		<Filters
    			filters={filters}
    			selectedFilters={this.state.selectedFilters}
    			state={this.state}
    			onUpdate={this.onUpdate} />

    		<Exercises
    			exercises={exercises}
    			selectedFilters={this.state.selectedFilters}
    			state={this.state}
    			onUpdate={this.onUpdate} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingHorizontal: 20,
    backgroundColor: '#262F38',
  }
})
