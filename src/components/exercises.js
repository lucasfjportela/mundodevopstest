import React, { Component } from 'react'
import { View, StyleSheet, FlatList, Image, Text, TouchableOpacity } from 'react-native'
import _ from 'lodash'

const iconBike = require('mundodevopstest/assets/images/ic_bike.png')
const iconBalance = require('mundodevopstest/assets/images/ic_balance.png')
const iconTime = require('mundodevopstest/assets/images/ic_time.png')

export default class Exercises extends Component {
	constructor (props) {
		super(props)

		this.state = {
			exercises: this.props.exercises
		}

		this.renderExerciseItem = this._renderExerciseItem.bind(this);
    this.renderNoneExercises = this._renderNoneExercises.bind(this);
	}

  renderTime (time) {
    const hour = time / 60

    if ((time % 60) === 0) {
      return (
        <Text style={[styles.infoText, {paddingLeft: 5}]}>{hour} h</Text>
      )
    } else {
      return (
        <Text style={[styles.infoText, {paddingLeft: 5}]}>{time} m</Text>
      )
    }
  }

  renderWhen (when, id) {
    return (
      <View style={{flexDirection: 'row', paddingVertical: 5}}>
        <TouchableOpacity
          style={[styles.whenButton, {
            backgroundColor: when === 'hoje' ? '#FD3C29' : 'transparent',
            borderColor: when === 'hoje' ? '#FD3C29' : 'gray'
          }]}>
          <Text style={[styles.whenText, {
            color: when === 'hoje' ? '#FEFFFF' : 'gray'
          }]}>HOJE</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.whenButton, {
            backgroundColor: when === 'ontem' ? '#19B996' : 'transparent',
            borderColor: when === 'ontem' && when ? '#19B996' : 'gray'
          }]}>
          <Text style={[styles.whenText, {
            color: when === 'ontem' ? '#FEFFFF' : 'gray'
          }]}>ONTEM</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _renderNoneExercises () {
    return (
      <View style={styles.noneExercisesView}>
        <Text style={styles.noneExercisesText}>Não há exercícios para esta categoria.</Text>
      </View>
    )
  }

  _renderExerciseItem (info) {
    return (
      <View style={styles.exercisesList}>
        <View style={styles.exerciseItem}>
          <View style={styles.imageView}>
            <Image style={{bottom: 5}} source={info.item.image} />
          </View>
          <View style={styles.exerciseInfoView}>
            <Text style={styles.exerciseTitle}>{info.item.name.toUpperCase()}</Text> 

            <View style={styles.row}>
              <View style={styles.exerciseInfo}>
                <Image style={styles.icon} source={iconBike} />
                <Text style={styles.infoText}> {info.item.calories} Kcal</Text>
              </View>
              <View style={styles.exerciseInfo}>
                <Image style={styles.icon} source={iconTime} />
                {this.renderTime(info.item.time)}
              </View>
              <View style={styles.exerciseInfo}>
                <Image style={styles.icon} source={iconBalance} />
                <Text style={styles.infoText}> {info.item.weight} Kg</Text>
              </View>
            </View>
            {this.renderWhen(info.item.when, info.index)}
          </View>
        </View>
      </View>
    )
  }

	render () {
		let filteredItems = _.filter(this.state.exercises, item => {
      const arr = this.props.selectedFilters
      if (_.isEmpty(arr)) { return item }
      if (arr.includes(item.categorie)) { return item }
    })

		return (
			<View>
				<FlatList
      	  style={styles.root}
      	  extraData={this.props.state}
      	  showsVerticalScrollIndicator={false}
      	  data={filteredItems}
      	  renderItem={this.renderExerciseItem}
          keyExtractor={(item, index) => index.toString()}
      	  ListEmptyComponent={this.renderNoneExercises} />
     	</View>
    )
  }
}

const styles = StyleSheet.create({
	root: {
		height: '100%'
	},
	noneExercisesView: {
		height: 50,
		justifyContent: 'center'
	},
	noneExercisesText: {
		fontFamily: 'Montserrat-Medium',
		fontSize: 16,
		color: '#FEFFFF',
		textAlign: 'center'
	},
	exercisesList: {
    height: 120,
    marginBottom: 20,
    marginTop: 10,
    padding: 15,
    backgroundColor: '#323C47',
    borderRadius: 10,
  },
  exerciseItem: {
  	justifyContent: 'space-between',
  	alignItems: 'center',
  	flexDirection: 'row'
	},
	imageView: {
		backgroundColor: '#2C343F',
		justifyContent: 'center',
		alignItems: 'center',
		height: 90,
		width: 90,
		borderRadius: 45
	},
	exerciseInfoView: {
		right: 0,
		height: 90,
		width: '62%'
	},
	exerciseTitle: {
		fontFamily: 'Montserrat-SemiBold',
		fontSize: 20,
		color: '#FEFFFF'
	},
	row: {
		flexDirection: 'row',
		paddingVertical: 10
	},
	exerciseInfo: {
		flexDirection: 'row',
		paddingRight: 15,
		alignItems: 'center'
	},
	icon: {
		height: 15,
		width: 15
	},
	infoText: {
		fontFamily: 'MontserratAlternates-SemiBold',
		fontSize: 12,
		color: '#FEFFFF'
	},
  whenButton: {
    marginLeft: 15,
    borderWidth: 1,
    width: 80,
    borderRadius: 35
  },
  whenText: {
    fontSize: 12,
    textAlign: 'center',
    fontFamily: 'Montserrat-Medium'
  }
})