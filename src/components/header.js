import React, { Component } from 'react'
import { View, Alert, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Header extends Component {
	showAlert (kind) {
    Alert.alert(`Ação: abrir ${kind}`)
  }

	render () {
		return (
			<View style={styles.header}>
        <TouchableOpacity onPress={() => this.showAlert('menu')}>
          <Icon
            size={25}
            name={'menu'} 
            color={'#fff'} />
        </TouchableOpacity>

        <Text style={styles.mainTitle}>MEU PERFIL</Text>

        <TouchableOpacity onPress={() => this.showAlert('configurações')}>
          <Icon
            size={25}
            name={'brightness-high'}
            color={'#fff'} />
        </TouchableOpacity>
      </View>
		)
	}
}

const styles = StyleSheet.create({
	header: {
    marginBottom: 15,
    borderBottomWidth: 1,
    borderColor: '#323C47',
    height: 80,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  mainTitle: {
    fontFamily: 'Montserrat-Light',
    fontSize: 26,
    color: '#FEFFFF',
    textAlign: 'center'
  }
})