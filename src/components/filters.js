import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Image, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import LinearGradient from 'react-native-linear-gradient'

export default class Filters extends Component {
	constructor (props) {
		super(props)

		this.state = {
			filters: this.props.filters
		}

		this.renderFilterItem = this._renderFilterItem.bind(this);
	}

	actionFilters (filter) {
    let array = this.props.selectedFilters

    if (array.includes(filter)) {
      array.splice(array.indexOf(filter), 1);
      this.props.onUpdate('selectedFilters', array)
    } else {
      array.push(filter)
      this.props.onUpdate('selectedFilters', array)
    }
  }

  selectedFilters (filter) {
    if (this.props.selectedFilters.includes(filter)) {
      return (
        <View style={styles.done}>
          <Icon
            size={15}
            name={'done'} 
            color={'white'} />
        </View>
      )
    }
  }

	_renderFilterItem (info) {
    return (
      <View style={{alignSelf: 'center'}}>
        <TouchableOpacity onPress={() => this.actionFilters(info.item.name)}>
          <LinearGradient
            start={{x: 1, y: 1}} end={{ x: -1, y: -1 }}
            locations={[0, 0.4]}
            colors={['#F22B48', '#7F38F4']}
            style={styles.overlay}>
            <Image source={info.item.image} />
          </LinearGradient>
        </TouchableOpacity>
        {this.selectedFilters(info.item.name)}
      </View>
    )
  }

	render () {
		return (
			<View style={styles.filtersList}>
        <FlatList
          horizontal
          extraData={this.props.state}
          showsHorizontalScrollIndicator={false}
          data={this.state.filters}
          renderItem={this.renderFilterItem}
          keyExtractor={(item, index) => index.toString()}/>
      </View>
		)
	}
}

const styles = StyleSheet.create({
	filtersList: {
    padding: 10,
    height: 95,
    width: '100%',
    backgroundColor: '#323C47',
    borderRadius: 10,
    marginBottom: 30
  },
  overlay: {
    height: 68,
    width: 68,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    borderRadius: 34
  },
  done: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#19B996',
    borderWidth: 1.5,
    borderColor: 'white',
    position: 'absolute',
    right: 5,
    width: 20,
    height: 20,
    borderRadius: 10
  }
})